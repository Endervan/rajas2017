<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",4) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  166px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- PRODUTOS GERAL  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row  top50 bottom15">

      <div class="col-xs-12 text-center">
        <img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_produtos.png" alt="">
        <h4>CONHEÇA NOSSOS PRODUTOS</h4>
        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/seta_geral.png" alt="">
      </div>

      <div class="col-xs-6 top90">
        <div class="breadcrumb">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a class="active">PRODUTOS</a>
        </div>
      </div>

      <div class="col-xs-6 central_dicas top90">
        <div class="pull-right">
          <div class="media-left media-middle ">
            <h4><span>central de atendimento:</span></h4>
          </div>
          <div class="media-body">
            <h5 class="media-heading">
              <span><?php Util::imprime($config[ddd1]) ?></span>
              <?php Util::imprime($config[telefone1]) ?>
            </h5>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<!-- ======================================================================= -->
<!-- PRODUTOS GERAL  -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!--PRODUTOS home    -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row dicas_geral pb40">
    <div class="sombra_dicas_geral"></div>
    <?php
    $result = $obj_site->select("tb_produtos");
    if(mysql_num_rows($result) > 0)
    {
      $i = 0;
      while($row = mysql_fetch_array($result))
      {
        ?>

        <div class="col-xs-6 lista_produto relativo top20">
          <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 574, 335, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
          </a>
          <h1 class="top15 lato_black"><?php Util::imprime($row[titulo]); ?></h1>
          <div class="top15 descricao_prod_home">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
          <div class="top15 ">
            <img src="./imgs/barra_home.jpg" alt="">
          </div>


          <div class="produto-hover text-center">
            <div class="col-xl-12">
              <h1><?php Util::imprime($row[titulo]); ?></h1>
            </div>
            <div class="col-xs-12">
              <a class="btn btn_saiba_mais" href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                SAIBA MAIS
              </a>
            </div>
            <div class="col-xs-12 top20">
              <a class="btn btn_saiba_mais" href="javascript:void(0);" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                ADICIONAR AO ORÇAMENTO
              </a>
            </div>
          </div>

        </div>

        <?php
        if ($i == 1) {
          echo '<div class="clearfix"></div>';
          $i = 0;
        }else{
          $i++;
        }
      }
    }
    ?>


  </div>
</div>
<!-- ======================================================================= -->
<!-- PRODUTOS home    -->
<!-- ======================================================================= -->

<div class="clearfix"></div>
<div class="top105"></div>

<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<!-- ======================================================================= -->
<!-- hover nos produtos   -->
<!-- ======================================================================= -->
<script type="text/javascript">
$(document).ready(function() {

  $(".produto-hover").hover(
    function () {
      $(this).stop().animate({opacity:0.5});
    },
    function () {
      $(this).stop().animate({opacity:0});
    }
  );

});
</script>
