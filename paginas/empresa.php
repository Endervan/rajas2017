<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  166px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- DICAS  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top50 bottom15">
      <div class="col-xs-12 text-center">
        <img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_empresa.png" alt="">
        <h4>CONHEÇA MAIS NOSSA EMPRESA</h4>
        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/seta_geral.png" alt="">
      </div>

      <div class="col-xs-6 top90">
        <div class="breadcrumb">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a class="active">A EMPRESA</a>
        </div>
      </div>

      <div class="col-xs-6 central_dicas top90">
        <div class="pull-right">
          <div class="media-left media-middle ">
            <h4><span>central de atendimento:</span></h4>
          </div>
          <div class="media-body">
            <h5 class="media-heading">
              <span><?php Util::imprime($config[ddd1]) ?></span>
              <?php Util::imprime($config[telefone1]) ?>
            </h5>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<!-- ======================================================================= -->
<!-- DICAS  -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- DICAS  -->
<!-- ======================================================================= -->
<div class="container bottom90">
  <div class="row dicas_geral">
    <div class="sombra_dicas_geral"></div>

    <div class="col-xs-12">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>

      <div class="top15">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>
    </div>

    <div class="clearfix"></div>
    <div class="top90"></div>
  </div>
</div>

<!-- ======================================================================= -->
<!-- DICAS  -->
<!-- ======================================================================= -->


<div class="container">
  <div class="row ">

    <!-- ======================================================================= -->
    <!-- MISSÃO  -->
    <!-- ======================================================================= -->
    <div class="col-xs-7">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
      <div class="top15">
        <h3><?php Util::imprime($row[titulo]); ?></h3>
      </div>
      <div class="top15">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>

    <!-- ======================================================================= -->
    <!-- MISSÃO  -->
    <!-- ======================================================================= -->

    <!-- ======================================================================= -->
    <!-- visão  -->
    <!-- ======================================================================= -->

      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
      <div class="top15">
        <h3><?php Util::imprime($row[titulo]); ?></h3>
      </div>
      <div class="top15">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>

    <!-- ======================================================================= -->
    <!-- visão  -->
    <!-- ======================================================================= -->

    <!-- ======================================================================= -->
    <!-- valores  -->
    <!-- ======================================================================= -->

      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
      <div class="top15">
        <h3><?php Util::imprime($row[titulo]); ?></h3>
      </div>
      <div class="top15">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>

      <?php
      $result = $obj_site->select("tb_atuacoes","LIMIT 5");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
          ?>
          <div class="col-xs-6 padding0  top20 lato_black">
            <p><i class="fa fa-check-square-o right10" aria-hidden="true"></i><?php Util::imprime($row[titulo]); ?></p>
          </div>
          <?php
        }
      }
      ?>

  </div>
    <!-- ======================================================================= -->
    <!-- valores  -->
    <!-- ======================================================================= -->


    <div class="col-xs-5 top60 padding0">
    <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg_empresa_predio.jpg" alt="">
    </div>


    <div class="clearfix"></div>
    <div class="top20"></div>
  </div>
</div>

<!-- ======================================================================= -->
<!--NOSSOS CLIENTES -->
<!-- ======================================================================= -->
<div class="container bottom50">
  <div class="row">
    <div class="col-xs-12 padding0">
      <h3 class="left15">NOSSOS CLIENTES</h3>
      <!-- ======================================================================= -->
      <!--SLIDER CLIENTES -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/slider_clientes.php'); ?>
      <!-- ======================================================================= -->
      <!--SLIDER CLIENTES -->
      <!-- ======================================================================= -->
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- NOSSOS CLIENTES -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!--NOSSOS FORNECEDORES -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row">
    <div class="col-xs-12 top30 bottom40">
      <h3 class="left15">NOSSOS FORNECEDORES</h3>
      <!-- ======================================================================= -->
      <!--SLIDER FORNECEDORES -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/slider_fornecedores.php'); ?>
      <!-- ======================================================================= -->
      <!--SLIDER FORNECEDORES -->
      <!-- ======================================================================= -->
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- NOSSOS FORNECEDORES -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
