<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  166px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- DICAS  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top50 bottom15">
      <div class="col-xs-12 text-center">
        <img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_dicas.png" alt="">
        <h4>CONHEÇA NOSSAS DICAS</h4>
        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/seta_geral.png" alt="">
      </div>

      <div class="col-xs-6 top90">
        <div class="breadcrumb">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a class="active">DICAS</a>
        </div>
      </div>

      <div class="col-xs-6 central_dicas top90">
        <div class="pull-right">
          <div class="media-left media-middle ">
            <h4><span>central de atendimento:</span></h4>
          </div>
          <div class="media-body">
            <h5 class="media-heading">
              <span><?php Util::imprime($config[ddd1]) ?></span>
              <?php Util::imprime($config[telefone1]) ?>
            </h5>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<!-- ======================================================================= -->
<!-- DICAS  -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- DICAS  -->
<!-- ======================================================================= -->
<div class="container bottom90">
  <div class="row dicas_geral">
    <div class="sombra_dicas_geral"></div>

    <?php
    $result = $obj_site->select("tb_dicas","ORDER BY RAND()");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        ?>
        <div class="col-xs-4 div_personalizada top20 dicas_home">
          <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 250, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
            <div class="lista-dica-faixa"></div>
          </a>
          <div class="lista_dica_desc">
            <h1><?php Util::imprime($row[titulo]); ?></h1>
          </div>
        </div>
        <?php
      }
    }
    ?>
    <div class="clearfix"></div>
    <div class="top90"></div>
  </div>
</div>

<!-- ======================================================================= -->
<!-- DICAS  -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
