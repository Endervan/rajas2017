<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/dicas");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",3) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  166px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- DICAS  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top50 bottom15">
      <div class="col-xs-12 text-center">
        <img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_dicas.png" alt="">
        <h3><span>Visando sempre a satisfação e bem estar dos clientes, sua infraestrutura</span></h3>
        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/seta_geral.png" alt="">
      </div>

      <div class="col-xs-6 top90">
        <div class="breadcrumb">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a class="active">DICA</a>


        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- DICAS  -->
  <!-- ======================================================================= -->


    <!-- ======================================================================= -->
    <!--DICAS DENTRO-->
    <!-- ======================================================================= -->
    <div class='container'>
      <div class="row top10">

        <div class="col-xs-8 dicas_Dentro">
          <div class="sombra_dicas_Dentro"></div>
          <div class="top30">
            <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
          </div>

            <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]",775, 280, array("class"=>"input100", "alt"=>"$dados_dentro[titulo]")) ?>

            <div class="top25">
              <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
            </div>

        </div>

        <div class="col-xs-4 top210 padding0">
        <h3 class="left15 top20">NOSSOS CLIENTES</h3>
          <?php
          $result = $obj_site->select("tb_dicas","ORDER BY RAND() limit 5");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              ?>
              <div class="col-xs-12 top35 dicas_home">
                <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 250, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                  <div class="lista-dica-faixa"></div>
                </a>
                <div class="lista_dica_desc">
                  <h1><?php Util::imprime($row[titulo]); ?></h1>
                </div>
              </div>
              <?php
            }
          }
          ?>
        </div>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!--DICAS DENTRO-->
    <!-- ======================================================================= -->



<div class="top105"></div>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
