<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna1{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  166px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- DICAS  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top50 bottom15">
      <div class="col-xs-12 text-center">
        <h3>PORTAL DO CLIENTE</h3>
        <p>Acompanhe o andamento de sua obra.</p>
      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- DICAS  -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- FORMULARIO  -->
  <!-- ======================================================================= -->
  <div class="container">
      <div class="row">
          <div class="col-xs-12">
              <form class="form-inline" method="post" action="">
                  <div class="form-group">
                    <label for="exampleInputName2">Digite o código da obra</label>
                    <input type="text" class="form-control right20 left20" name="codigo_obra" placeholder="">
                  </div>
                  <button type="submit" class="btn btn_formulario" style="padding: 5px 10px;">Enviar</button>
                </form>
          </div>
      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- FORMULARIO  -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- RESULTADO  -->
  <!-- ======================================================================= -->
  <?php
  if (isset($_POST[codigo_obra])){
      $result = $obj_site->select("tb_obras", "and codigo_obra = '$_POST[codigo_obra]' ");

      if(mysql_num_rows($result) == 0){
    ?>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center top30">
                    <h3>Nenhuma obra encontrada com o código informado.</h3>
                </div>
            </div>
        </div>
    <?php
      }else{
          $dados = mysql_fetch_array($result);
          ?>
            <div class="container top50">
                <div class="row lista-detalhe-obra">

                    <div class="col-xs-12">
                        <div class="alert alert-info" role="alert">
                          <?php echo Util::imprime($dados[titulo]) ?>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <h5>Código da obra</h5>
                        <p><?php echo Util::imprime($dados[codigo_obra]) ?></p>
                    </div>

                    <div class="col-xs-3">
                        <h5>Nota fiscal</h5>
                        <p><?php echo Util::imprime($dados[nota_fiscal]) ?></p>
                    </div>

                    <div class="col-xs-3">
                        <h5>Data de início</h5>
                        <p><?php echo Util::formata_data($dados[data_inicio]) ?></p>
                    </div>

                    <div class="col-xs-3">
                        <h5>Data de Entrega</h5>
                        <p><?php echo Util::formata_data($dados[data_entrega]) ?></p>
                    </div>

                    <div class="clearfix"></div>
                    <div class="top20"></div>

                    <div class="col-xs-4">
                        <h5>Medição</h5>
                        <div class="progress">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[medicao]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[medicao]) ?>%;">
                            <?php echo Util::imprime($dados[medicao]) ?> %
                          </div>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <h5>Material</h5>
                        <div class="progress">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[material]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[material]) ?>%;">
                            <?php echo Util::imprime($dados[material]) ?> %
                          </div>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <h5>Lista de Corte</h5>
                        <div class="progress">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[lista_corte]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[lista_corte]) ?>%;">
                            <?php echo Util::imprime($dados[lista_corte]) ?> %
                          </div>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <h5>Lista de Vidro</h5>
                        <div class="progress">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[lista_vidro]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[lista_vidro]) ?>%;">
                            <?php echo Util::imprime($dados[lista_vidro]) ?> %
                          </div>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <h5>Produção (corte e montagem)</h5>
                        <div class="progress">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[producao]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[producao]) ?>%;">
                            <?php echo Util::imprime($dados[producao]) ?> %
                          </div>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <h5>Instalação</h5>
                        <div class="progress">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[instalacao]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[instalacao]) ?>%;">
                            <?php echo Util::imprime($dados[instalacao]) ?> %
                          </div>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <h5>Revisão Final</h5>
                        <div class="progress">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[revisao_final]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[revisao_final]) ?>%;">
                            <?php echo Util::imprime($dados[revisao_final]) ?> %
                          </div>
                        </div>
                    </div>

                    <?php if(!empty($dados[link_driver_contratos])): ?>
                    <div class="col-xs-4">
                        <h5>Contrato(s)</h5>
                        <a href="<?php echo Util::imprime($dados[link_driver_contratos]) ?>" target="_blank">Vizualizar</a>
                    </div>
                    <?php endif; ?>

                    <?php if(!empty($dados[link_driver_croqui])): ?>
                    <div class="col-xs-4">
                        <h5>Croqui</h5>
                        <a href="<?php echo Util::imprime($dados[link_driver_croqui]) ?>" target="_blank">Vizualizar(s)</a>
                    </div>
                    <?php endif; ?>



                </div>
            </div>
          <?php
      }
  }
  ?>
  <!-- ======================================================================= -->
  <!-- RESULTADO  -->
  <!-- ======================================================================= -->







  <div class="top105"></div>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>



<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{
  $texto_mensagem = "
  Nome: ".($_POST[nome])." <br />
  Localidade: ".($_POST[localidade])." <br />
  Telefone: ".($_POST[telefone])." <br />
  Email: ".($_POST[email])." <br />
  Mensagem: <br />
  ".(nl2br($_POST[mensagem]))."
  ";


  Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);

  Util::alert_bootstrap("Obrigado por entrar em contato.");
  unset($_POST);
}


?>



<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Insira seu nome.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Informe um email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor informe seu numero!.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto1: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
