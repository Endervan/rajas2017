<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 3");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- EMPRESA DIFERENCAS -->
  <!-- ======================================================================= -->
  <div class="container bg_diferencas">
    <div class="row">
      <div class="col-xs-8">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
        <div class="top45">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>
      </div>
      <div class="col-xs-4 text-right top15">
        <h1 class="right20"><span><?php Util::imprime($config[ddd1]) ?></span><?php Util::imprime($config[telefone1]) ?></h1>
      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- EMPRESA DIFERENCAS -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--PRODUTOS home    -->
  <!-- ======================================================================= -->
  <div class="container top20">
    <div class="row ">
      <div class="col-xs-12 text-center lato_black top30">
        <h2>CONHEÇA NOSSOS PRODUTOS</h2>
        <img class="clearfix top15" src="./imgs/arrow_donw_home.png" alt="" />
      </div>
      <div class="clearfix"></div>
      <?php
      $result = $obj_site->select("tb_produtos","AND exibir_home = 'SIM' ORDER BY RAND() LIMIT 4");
      if(mysql_num_rows($result) > 0)
      {
        $i = 0;
        while($row = mysql_fetch_array($result))
        {
          ?>

          <div class="col-xs-6 lista_produto relativo top40">
            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 574, 335, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
            </a>
            <h1 class="top15 lato_black"><?php Util::imprime($row[titulo]); ?></h1>
            <div class="top15 descricao_prod_home">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
            <div class="top15 ">
              <img src="./imgs/barra_home.jpg" alt="">
            </div>


            <div class="produto-hover text-center">
              <div class="col-xl-12">
                <h1><?php Util::imprime($row[titulo]); ?></h1>
              </div>
              <div class="col-xs-12">
                <a class="btn btn_saiba_mais" href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  SAIBA MAIS
                </a>
              </div>
              <div class="col-xs-12 top20">
                <a class="btn btn_saiba_mais" href="javascript:void(0);" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                  ADICIONAR AO ORÇAMENTO
                </a>
              </div>
            </div>

          </div>

          <?php
          if ($i == 1) {
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }
        }
      }
      ?>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- PRODUTOS home    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- EMPRESA HOME  -->
  <!-- ======================================================================= -->
  <div class="container-fluid top40">
    <div class="row bg_home_empresa">
      <div class="container">
        <div class="row">
          <div class="col-xs-8 col-xs-offset-4">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
            <div class="desc_empresa_home">
              <p class="top15"><?php Util::imprime($row[descricao]); ?></p>
            </div>

            <?php
            $result = $obj_site->select("tb_atuacoes","LIMIT 5");
            if(mysql_num_rows($result) > 0){
              while($row = mysql_fetch_array($result)){
                ?>
                <div class="col-xs-6 padding0  top20 lato_black">
                  <p><i class="fa fa-check-square-o right10" aria-hidden="true"></i><?php Util::imprime($row[titulo]); ?></p>
                </div>
                <?php
              }
            }
            ?>

          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- EMPRESA HOME  -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--NOSSOS CLIENTES -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 top30 padding0">
        <h3 class="left15">NOSSOS CLIENTES</h3>
        <!-- ======================================================================= -->
        <!--SLIDER CLIENTES -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/slider_clientes.php'); ?>
        <!-- ======================================================================= -->
        <!--SLIDER CLIENTES -->
        <!-- ======================================================================= -->
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- NOSSOS CLIENTES -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- DICAS  -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row bg_home_dicas top50">
      <div class="container">
        <div class="row top50">
          <div class="col-xs-12">
            <h3>NOSSAS DICAS</h3>
          </div>

          <?php
          $result = $obj_site->select("tb_dicas","ORDER BY RAND() LIMIT 2");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              ?>
              <div class="col-xs-4 top30 dicas_home">
                <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 250, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                  <div class="lista-dica-faixa"></div>
                </a>
                <div class="lista_dica_desc">
                  <h1><?php Util::imprime($row[titulo]); ?></h1>
                </div>
              </div>
              <?php
            }
          }
          ?>

        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- DICAS  -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!--NOSSOS FORNECEDORES -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 top30 bottom40">
        <h3 class="left15">NOSSOS FORNECEDORES</h3>
        <!-- ======================================================================= -->
        <!--SLIDER FORNECEDORES -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/slider_fornecedores.php'); ?>
        <!-- ======================================================================= -->
        <!--SLIDER FORNECEDORES -->
        <!-- ======================================================================= -->
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- NOSSOS FORNECEDORES -->
  <!-- ======================================================================= -->


  <div class="clearfix"></div>

  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">


<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: true,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'none',  //  Navigation type, can be 'bullets', 'thumbnails', 'tabs' or 'none'
    autoScaleSlider: false,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: true,
    loopRewind: true,
    numImagesToPreload: 3,
    keyboardNavEnabled: true,
    usePreloader: false,
    transitionSpeed: 1000,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      delay: 3000,
      pauseOnHover: true
    }

  });
});
</script>

<?php require_once('./includes/js_css.php') ?>


<!-- ======================================================================= -->
<!-- hover nos produtos   -->
<!-- ======================================================================= -->
<script type="text/javascript">
$(document).ready(function() {

  $(".produto-hover").hover(
    function () {
      $(this).stop().animate({opacity:0.5});
    },
    function () {
      $(this).stop().animate({opacity:0});
    }
  );

});
</script>
