<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem]); ?>) top  166px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row prod-dentro-titulo_1 top50">
      <div class="col-xs-12 text-center ">
        <div class=" text-center">
          <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
          <a href="javascript:void(0);" class="btn btn-transparente top5" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">ADICIONAR AO ORÇAMENTO</a>
        </div>
      </div>
    </div>
  </div>




  <!-- ======================================================================= -->
  <!-- PRODUTOS GERAL  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top40">


      <div class="col-xs-6">
        <div class="breadcrumb">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a href="<?php echo Util::caminho_projeto(); ?>/produtos">PRODUTOS</a>
          <a class="active"><?php Util::imprime($dados_dentro[titulo],30); ?></a>
        </div>
      </div>

      <div class="col-xs-6 central_dicas">
        <div class="pull-right">
          <div class="media-left media-middle ">
            <h4><span>central de atendimento:</span></h4>
          </div>
          <div class="media-body">
            <h5 class="media-heading">
              <span><?php Util::imprime($config[ddd1]) ?></span>
              <?php Util::imprime($config[telefone1]) ?>
            </h5>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<!-- ======================================================================= -->
<!-- PRODUTOS GERAL  -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!--PRODUTOS home    -->
<!-- ======================================================================= -->
<div class="container top20">
  <div class="row dicas_geral_dentro_1 pb40">
    <div class="sombra_dicas_geral_1"></div>



    <div class="col-xs-7">
        <?php require_once('./includes/slider_produto_interno.php'); ?>
        <div class="top20">
          <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
        </div>
    </div>

    <div class="col-xs-5 top15">

        <h1>CONSULTE O VALOR EM</h1>

          <div class="top20 left15">
            <h2>
             <i class="fa fa-phone" aria-hidden="true"></i>    
              <span><?php Util::imprime($config[ddd1]) ?></span><?php Util::imprime($config[telefone1]) ?>
            </h2>
            <?php if (!empty($config[telefone2])): ?>
              <h2 class="top10">
                  <i class="fa fa-phone" aria-hidden="true"></i>
                <span><?php Util::imprime($config[ddd2]) ?></span><?php Util::imprime($config[telefone2]) ?>
              </h2>
              <?php endif; ?>

          </div>

          <div class="top30">
            <a href="javascript:void(0);" class="btn btn_orcamento lato_black" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">ADICIONAR AO ORÇAMENTO</a>
          </div>


    </div>





    <div class="clearfix">


    <div class="col-xs-12 text-center top30">
      <h2>CONFIRA ALGUNS DO SERVIÇOS REALIZADOS</h2>
    </div>
    <!-- ======================================================================= -->
    <!--SLIDER SERVICOS -->
    <!-- ======================================================================= -->
    <div class="col-xs-10  col-xs-offset-1 padding0">
    <?php require_once('./includes/slider_servicos.php'); ?>
    </div>
    <!-- ======================================================================= -->
    <!--SLIDER SERVICOS -->
    <!-- ======================================================================= -->


    </div>
    <div class=" col-xs-12 text-center">
      <a href="javascript:void(0);" class="btn btn_orcamento lato_black" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">ADICIONAR AO ORÇAMENTO</a>
    </div>

  </div>
</div>
<!-- ======================================================================= -->
<!-- PRODUTOS home    -->
<!-- ======================================================================= -->




<div class="clearfix"></div>
<div class="top105"></div>

<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">

<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1-produtos').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: true,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',  //  Navigation type, can be 'bullets', 'thumbnails', 'tabs' or 'none'
    autoScaleSlider: false,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: true,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    transitionSpeed: 1000,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      delay: 3000,
      pauseOnHover: true
    }

  });
});
</script>


<?php require_once('./includes/js_css.php') ?>



<script>
$(window).load(function() {
  // The slider being synced must be initialized first
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 200,
    itemMargin: 5,
    asNavFor: '#slider'
  });

  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
});


</script>
