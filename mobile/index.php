<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>
</head>

<body>
  <?php require_once('./includes/topo.php'); ?>



  <!-- ======================================================================= -->
  <!-- slider	-->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row carroucel-home">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <?php
          $result = $obj_site->select("tb_banners", "and tipo_banner = 2 ");
          if(mysql_num_rows($result) > 0){
            $i = 0;
            while ($row = mysql_fetch_array($result)) {
              $imagens[] = $row;
              ?>
              <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
              <?php
              $i++;
            }
          }
          ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <?php
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
              ?>
              <div class="item <?php if($i == 0){ echo "active"; } ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="First slide">
                <div class="carousel-caption text-center">
                  <?php /*<h1><?php Util::imprime($imagem[titulo]); ?></h1>
                  <p><?php Util::imprime($imagem[legenda]); ?></p>*/ ?>
                  <?php if (!empty($imagem[url])) { ?>
                    <a class="btn btn-transparente right15" href="<?php Util::imprime($imagem[url]); ?>" role="button">SAIBA MAIS</a>
                    <?php } ?>
                  </div>
                </div>

                <?php
                $i++;
              }
            }
            ?>


          </div>



          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>



        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- slider	-->
    <!-- ======================================================================= -->



    <!-- ======================================================================= -->
    <!-- EMPRESA DIFERENCAS -->
    <!-- ======================================================================= -->
    <div class="container bg_diferencas">
      <div class="row">
        <div class="col-xs-12 ">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
          <div class="top10 pg10">
            <p><?php Util::imprime($row[descricao],1000); ?></p>
          </div>
        </div>

      </div>
    </div>

    <!-- ======================================================================= -->
    <!-- EMPRESA DIFERENCAS -->
    <!-- ======================================================================= -->





    <!-- ======================================================================= -->
    <!--PRODUTOS home    -->
    <!-- ======================================================================= -->
    <div class="container top20">
      <div class="row ">
        <div class="col-xs-12 text-center lato_black top5">
          <h2>CONHEÇA NOSSOS PRODUTOS</h2>
          <img class="clearfix top15" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/arrow_donw_home.png" alt="" />
        </div>
        <div class="clearfix"></div>


        <div class="col-xs-12 top10 padding0">

          <!-- ======================================================================= -->
          <!--SLIDER CLIENTES -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/slider_produtos.php'); ?>
          <!-- ======================================================================= -->
          <!--SLIDER CLIENTES -->
          <!-- ======================================================================= -->
        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- NOSSOS CLIENTES -->
    <!-- ======================================================================= -->


    <!-- ======================================================================= -->
    <!-- EMPRESA HOME  -->
    <!-- ======================================================================= -->

    <div class="container top80">
      <div class="row bg_home_empresa">
        <div class="col-xs-8 col-xs-offset-4">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
          <div class="desc_empresa_home">
            <p class="top15"><?php Util::imprime($row[descricao]); ?></p>
          </div>

          <?php
          $result = $obj_site->select("tb_atuacoes","LIMIT 5");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              ?>
              <div class="col-xs-12 padding0  top10 lato_black">
                <p><i class="fa fa-check-square-o right10" aria-hidden="true"></i><?php Util::imprime($row[titulo]); ?></p>
              </div>
              <?php
            }
          }
          ?>

        </div>

      </div>
    </div>

    <!-- ======================================================================= -->
    <!-- EMPRESA HOME  -->
    <!-- ======================================================================= -->



    <!-- ======================================================================= -->
    <!--NOSSOS CLIENTES -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row">
        <div class="col-xs-12 top20 padding0">
          <h3 class="left15">NOSSOS CLIENTES</h3>
          <!-- ======================================================================= -->
          <!--SLIDER CLIENTES -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/slider_clientes.php'); ?>
          <!-- ======================================================================= -->
          <!--SLIDER CLIENTES -->
          <!-- ======================================================================= -->
        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- NOSSOS CLIENTES -->
    <!-- ======================================================================= -->

    <!-- ======================================================================= -->
    <!-- DICAS  -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row  top80">
        <div class="col-xs-12">
          <h3>NOSSAS DICAS</h3>
        </div>

        <?php
        $result = $obj_site->select("tb_dicas","ORDER BY RAND() LIMIT 2");
        if(mysql_num_rows($result) > 0){
          while($row = mysql_fetch_array($result)){
            ?>
            <div class="col-xs-6 top30 dicas_home">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 250, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                <div class="lista-dica-faixa"></div>
              </a>
              <div class="lista_dica_desc top15">
                <h1><?php Util::imprime($row[titulo]); ?></h1>
              </div>
            </div>
            <?php
          }
        }
        ?>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- DICAS  -->
    <!-- ======================================================================= -->



    <!-- ======================================================================= -->
    <!--NOSSOS FORNECEDORES -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row">
        <div class="col-xs-12 top20 bottom15">
          <h3 class="left15">NOSSOS FORNECEDORES</h3>
          <!-- ======================================================================= -->
          <!--SLIDER FORNECEDORES -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/slider_fornecedores.php'); ?>
          <!-- ======================================================================= -->
          <!--SLIDER FORNECEDORES -->
          <!-- ======================================================================= -->
        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- NOSSOS FORNECEDORES -->
    <!-- ======================================================================= -->


    <div class="clearfix"></div>



    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->


    <!-- ======================================================================= -->
    <!-- modal    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/modal.php') ?>
    <!-- ======================================================================= -->
    <!-- modal    -->
    <!-- ======================================================================= -->

  </body>

  </html>





  <?php require_once('./includes/js_css.php') ?>
