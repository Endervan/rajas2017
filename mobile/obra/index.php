
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9); ?>
<style>
.bg-interna1{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 140px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->





	<!-- ======================================================================= -->
	<!-- TITULO  -->
	<!-- ======================================================================= -->
	<div class="container">
	  <div class="row bottom40">
	    <div class="col-xs-12">
	      <div class="top25 text-center">
	        <h3>PORTAL DO CLIENTE</h3>
			<p class="top5">Acompanhe o andamento de sua obra.</p>
	      </div>
	    </div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- TITULO  -->
	<!-- ======================================================================= -->




	<!-- ======================================================================= -->
    <!-- FORMULARIO  -->
    <!-- ======================================================================= -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <form class="form-inline" method="post" action="">
                    <div class="form-group col-xs-10">
                      <label for="">Digite o código da obra</label>
                      <input type="text" class="form-control right10 " name="codigo_obra" placeholder="">
                    </div>
                    <button type="submit" class="btn btn_formulario" style="padding: 10px 10px; margin-top: 24px;">Enviar</button>
                  </form>
            </div>
        </div>
    </div>
    <!-- ======================================================================= -->
    <!-- FORMULARIO  -->
    <!-- ======================================================================= -->



	<!-- ======================================================================= -->
    <!-- RESULTADO  -->
    <!-- ======================================================================= -->
    <?php
    if (isset($_POST[codigo_obra])){
        $result = $obj_site->select("tb_obras", "and codigo_obra = '$_POST[codigo_obra]' ");

        if(mysql_num_rows($result) == 0){
      ?>
          <div class="container">
              <div class="row">
                  <div class="col-xs-12 text-center top30">
                      <h3>Nenhuma obra encontrada com o código informado.</h3>
                  </div>
              </div>
          </div>
      <?php
        }else{
            $dados = mysql_fetch_array($result);
            ?>
              <div class="container top50">
                  <div class="row lista-detalhe-obra">

                      <div class="col-xs-12">
                          <div class="alert alert-info" role="alert">
                            <?php echo Util::imprime($dados[titulo]) ?>
                          </div>
                      </div>

                      <div class="col-xs-6">
                          <h3>Código da obra</h3>
                          <p><?php echo Util::imprime($dados[codigo_obra]) ?></p>
                      </div>

                      <div class="col-xs-6">
                          <h3>Nota fiscal</h3>
                          <p><?php echo Util::imprime($dados[nota_fiscal]) ?></p>
                      </div>


                      <div class="col-xs-6 top20">
                          <h3>Data de início</h3>
                          <p><?php echo Util::formata_data($dados[data_inicio]) ?></p>
                      </div>

                      <div class="col-xs-6 top20">
                          <h3>Data de Entrega</h3>
                          <p><?php echo Util::formata_data($dados[data_entrega]) ?></p>
                      </div>



                      <div class="col-xs-12 top20">
                          <h3>Medição</h3>
                          <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[medicao]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[medicao]) ?>%;">
                              <?php echo Util::imprime($dados[medicao]) ?> %
                            </div>
                          </div>
                      </div>

                      <div class="col-xs-12">
                          <h3>Material</h3>
                          <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[material]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[material]) ?>%;">
                              <?php echo Util::imprime($dados[material]) ?> %
                            </div>
                          </div>
                      </div>

                      <div class="col-xs-12">
                          <h3>Lista de Corte</h3>
                          <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[lista_corte]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[lista_corte]) ?>%;">
                              <?php echo Util::imprime($dados[lista_corte]) ?> %
                            </div>
                          </div>
                      </div>

                      <div class="col-xs-12">
                          <h3>Lista de Vidro</h3>
                          <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[lista_vidro]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[lista_vidro]) ?>%;">
                              <?php echo Util::imprime($dados[lista_vidro]) ?> %
                            </div>
                          </div>
                      </div>

                      <div class="col-xs-12">
                          <h3>Produção (corte e montagem)</h3>
                          <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[producao]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[producao]) ?>%;">
                              <?php echo Util::imprime($dados[producao]) ?> %
                            </div>
                          </div>
                      </div>

                      <div class="col-xs-12">
                          <h3>Instalação</h3>
                          <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[instalacao]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[instalacao]) ?>%;">
                              <?php echo Util::imprime($dados[instalacao]) ?> %
                            </div>
                          </div>
                      </div>

                      <div class="col-xs-12">
                          <h3>Revisão Final</h3>
                          <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo Util::imprime($dados[revisao_final]) ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo Util::imprime($dados[revisao_final]) ?>%;">
                              <?php echo Util::imprime($dados[revisao_final]) ?> %
                            </div>
                          </div>
                      </div>

                      <?php if(!empty($dados[link_driver_contratos])): ?>
                      <div class="col-xs-12">
                          <h3>Contrato(s)</h3>
                          <a class="btn" href="<?php echo Util::imprime($dados[link_driver_contratos]) ?>" target="_blank">Vizualizar</a>
                      </div>
                      <?php endif; ?>

                      <?php if(!empty($dados[link_driver_croqui])): ?>
                      <div class="col-xs-12">
                          <h3>Croqui</h3>
                          <a class="btn" href="<?php echo Util::imprime($dados[link_driver_croqui]) ?>" target="_blank">Vizualizar(s)</a>
                      </div>
                      <?php endif; ?>



                  </div>
              </div>
            <?php
        }
    }
    ?>
    <!-- ======================================================================= -->
    <!-- RESULTADO  -->
    <!-- ======================================================================= -->



	<div class="top40">

	</div>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>




<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
