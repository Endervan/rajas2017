<?php
$qtd_itens_linha = 1; // quantidade de itens por linha
$id_slider = "carousel-example-generic-produtos";
unset($linha);
?>


<div id="<?php echo $id_slider ?>" class="carousel slide top20 slider-parceiros-produtos" data-ride="carousel" interval="555" pause="hover">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
    $result = $obj_site->select("tb_produtos","order by rand() limit 4");
    $linhas = mysql_num_rows($result) / $qtd_itens_linha;
    $b_temp = 0;

    for($i_temp = 0; $i_temp < $linhas; $i_temp++){
      ?>
      <li data-target="#<?php echo $id_slider ?>" data-slide-to="<?php echo $i_temp; ?>" class="<?php if($i_temp == 0){ echo "active"; } ?>"></li>
      <?php


      // guarda a qtd de linhas
      $result1 = $obj_site->select("tb_produtos", "limit $b_temp, $qtd_itens_linha");

      if(mysql_num_rows($result1) > 0){
        while($row1 = mysql_fetch_array($result1)){
          $linha[] = $row1;
        }
      }

      //  aumenta a qtd de itens a busca na consulta
      $b_temp = $b_temp + $qtd_itens_linha;
    }

    ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">

    <?php
    if (count($linhas) > 0) {
      $i_temp = 0;
      $b_temp = $qtd_itens_linha;
      $c_temp = 0;
      for($i_temp = 0; $i_temp < $linhas; $i_temp++){
        ?>
        <div class="item <?php if($i_temp == 0){ echo "active"; } ?>">

          <?php

          //  lista os itens da linha
          for($c_temp; $c_temp <$b_temp; $c_temp++){
            $imagem = $linha[$c_temp][imagem];

            if(!empty($linha[$c_temp][titulo])):
              ?>

              <div class="lista_parceiros_produtos">


                <div class="col-xs-12 text-center">
                  <!-- <img src="<?php //echo Util::caminho_projeto() ?>/uploads/<?php //Util::imprime($imagem); ?>" alt="" /> -->
                  <div class="bg_imagem">
                    <?php $obj_site->redimensiona_imagem("../uploads/$imagem", 450, 261, array("class"=>"input100 top15 bottom15", "alt"=>"$linha[$c_temp][titulo]") ) ?>

                  </div>
                </div>

                <div class="col-xs-12 dicas_descricao_home">
                  <div class="top15">
                    <h1><?php Util::imprime($linha[$c_temp][titulo]) ?></h1>
                  </div>
                  <div class="top10 desc_prod">
                    <p><?php Util::imprime($linha[$c_temp][descricao], 300) ?></p>
                  </div>
                </div>

                <div class="produto-hover text-center">

                  <div class="col-xs-4 top15">
                    <a class="btn btn_saiba_mais" href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($linha[$c_temp][url_amigavel]); ?>" title="<?php Util::imprime($linha[$c_temp][titulo]); ?>">
                      SAIBA MAIS
                    </a>
                  </div>
                  <div class="col-xs-8 top15">
                    <a class="btn btn_saiba_mais" href="javascript:void(0);" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($linha[$c_temp][0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($linha[$c_temp][0]) ?>, 'produto'">
                      ADICIONAR AO ORÇAMENTO
                    </a>
                  </div>
                </div>

              </div>

              <?php
            endif;
          }
          ?>

        </div>
        <?php
        $b_temp = $b_temp + $qtd_itens_linha;
      }
    }
    ?>

  </div>



  <!-- Controls -->
  <a class="left carousel-control" href="#<?php echo $id_slider ?>" role="button" data-slide="prev">
    <i class="fa fa-chevron-left" aria-hidden="true"></i>
  </a>
  <a class="right carousel-control" href="#<?php echo $id_slider ?>" role="button" data-slide="next">
    <i class="fa fa-chevron-right" aria-hidden="true"></i>
  </a>
</div>
