
<!-- ======================================================================= -->
<!-- MODAL CARRINHO  -->
<!-- ======================================================================= -->
<div class="modal fade" id="myModal-topo-itens-orcamento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Itens do Meu Orçamento</h4>
      </div>
      <div class="modal-body">

          <!--  ==============================================================  -->
          <!--sessao adicionar produtos-->
          <!--  ==============================================================  -->
          <?php
          if(count($_SESSION[solicitacoes_produtos]) > 0)
          {
            for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
            {
              $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
              ?>
              <div class="lista-itens-carrinho">
                <div class="col-xs-2">
                  <?php if(!empty($row[imagem])): ?>
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                  <?php endif; ?>
                </div>
                <div class="col-xs-8">
                  <p><?php Util::imprime($row[titulo]) ?></p>
                </div>
                <div class="col-xs-1">
                  <a class="btn" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                </div>
                <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>
              </div>
              <div class="clearfix"></div>
              <?php
            }
          }
          ?>


          <!--  ==============================================================  -->
          <!--sessao adicionar servicos-->
          <!--  ==============================================================  -->
          <?php
          if(count($_SESSION[solicitacoes_servicos]) > 0)
          {
            for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
            {
              $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
              ?>
              <div class="lista-itens-carrinho">
                <div class="col-xs-2">
                  <?php if(!empty($row[imagem])): ?>
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                  <?php endif; ?>
                </div>
                <div class="col-xs-8">
                  <p><?php Util::imprime($row[titulo]) ?></p>
                </div>
                <div class="col-xs-1">
                  <a class="btn" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                </div>
                <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>

              </div>
              <div class="clearfix"></div>
              <?php
            }
          }
          ?>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento" class="btn btn-primary">Finalizar Orçamento</a>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- MODAL CARRINHO  -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- MODAL MENU  -->
<!-- ======================================================================= -->
<div class="modal fade menu" id="myModal-menu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Menu</h4>
      </div>
      <div class="modal-body lista-menu text-left">

        <div class="list-group">
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile">HOME</a>
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">A EMPRESA</a>
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">PRODUTOS</a>
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/dicas">DICAS</a>
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">FALE CONOSCO</a>
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco">TRABALHE CONOSCO</a>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- MODAL MENU  -->
<!-- ======================================================================= -->





<!-- ======================================================================= -->
<!-- MODAL CATEGORIAS PRODUTO	-->
<!-- ======================================================================= -->
<div class="modal fade" id="myModal-lista-categorias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Selecione a Categoria desejada</h4>
      </div>
      <div class="modal-body">


        <div class="list-group">
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/">
              Ver Todos
          </a>

          <?php
          $result = $obj_site->select("tb_categorias_produtos");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
            ?>
                <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>">
                  <?php Util::imprime($row[titulo]); ?>
                </a>
            <?php
            }
          }
          ?>
          </div>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- MODAL CATEGORIAS PRODUTO	-->
<!-- ======================================================================= -->
