<div class="topo-geral">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 pt10 topo">
        <div class="pull-left">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/obra">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/portal_cliente.png" alt="">
          </a>
        </div>

        <div class="pull-right">
          <div class="media-left media-middle ">
            <h2 class="media-heading">
              <span><?php Util::imprime($config[ddd1]) ?></span>
              <?php Util::imprime($config[telefone1]) ?>
            </h2>
          </div>
          <div class="media-body">
            <a href="tel:+55<?php Util::imprime($config[ddd1]) ?> <?php Util::imprime($config[telefone1]) ?>" class="btn btn_transparente_topo">CHAMAR</a>
          </div>

        </div>

      </div>
    </div>



    <div class="container">
      <div class="row bg-topo top10">
        <div class="col-xs-4 logo text-center">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile">
            <img class="top5" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="">
          </a>
        </div>

        <div class="col-xs-4">

          <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal-menu">
            <img class="top10" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_menu.jpg" alt="">
          </a>

        </div>

        <div class="col-xs-4">
          <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal-topo-itens-orcamento">
            <img class="top10" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn_itens_orcamento.jpg" alt="">
          </a>

        </div>






        </div>
      </div>
    </div>
  </div>
