
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo",9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 140px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- TITULO EMPRESA  -->
	<!-- ======================================================================= -->
	<div class="container">
		<div class="row top50 bottom40">
			<div class="col-xs-12 text-center titulo_trabalhe">
				<img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_trabalhe.png" alt="">
				<h4>ENVIE SEU CÚRRICULO, TRABALHE CONOSCO</h4>
				<img class="top20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/arrow_donw_geral.png" alt="">
			</div>
		</div>

	</div>
</div>
<!-- ======================================================================= -->
<!-- TITULO EMPRESA  -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- DESC EMPRESA  -->
<!-- ======================================================================= -->
<div class="container">
	<div class="row dicas_geral_1">

		<div class="col-xs-12">
			<div class="dicas_geral pb20">
				<!--  ==============================================================  -->
				<!-- CONTATOS E ENDERENÇO-->
				<!--  ==============================================================  -->
				<div class="col-xs-12 top15">
					<div class="media-left media-middle ">
						<h2 class="media-heading">
							<span><?php Util::imprime($config[ddd1]) ?></span>
							<?php Util::imprime($config[telefone1]) ?>
						</h2>
					</div>
					<div class="media-body">
						<a href="tel:+55<?php Util::imprime($config[ddd1]) ?> <?php Util::imprime($config[telefone1]) ?>">
							<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn_chamar.jpg" alt="">
						</a>
					</div>
				</div>

				<div class="col-xs-2 top20">
					<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_mapa.png" alt="">
				</div>
				<div class="col-xs-10 top15">
					<div class="media-left media-middle ">
						<p class="media-heading">

							<?php Util::imprime($config[endereco]) ?>
						</p>
					</div>
					<div class="media-body">
						<a href="<?php Util::imprime($config[link_place]); ?>" class="" target="_blank">
							<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/abrir_mapa.jpg" alt="">
						</a>
					</div>
				</div>
				<!--  ==============================================================  -->
				<!-- CONTATOS E ENDERENÇO-->
				<!--  ==============================================================  -->
				<!--  ==============================================================  -->
				<!-- FORMULARIO-->
				<!--  ==============================================================  -->
				<div class="col-xs-12 padding0 top20">
					<h3 class="left15"><span>ENVIE  UM E-MAIL</span></h3>
					<form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
						<div class="fundo_formulario">
							<!-- formulario orcamento -->
							<div class="top5">
								<div class="col-xs-6">
									<div class="form-group  input100 has-feedback">
										<input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">

									</div>
								</div>

								<div class="col-xs-6">
									<div class="form-group  input100 has-feedback">
										<input type="text" name="email" class="form-control fundo-form1 input100 input-lg" placeholder="E-MAIL">

									</div>
								</div>
							</div>

							<div class="clearfix"></div>


							<div class="top5">
								<div class="col-xs-6">
									<div class="form-group input100 has-feedback ">
										<input type="text" name="telefone" class="form-control fundo-form1 input100  input-lg" placeholder="TELEFONE">

									</div>
								</div>

								<div class="col-xs-6">
									<div class="form-group input100 has-feedback ">
										<input type="text" name="escolaridade" class="form-control fundo-form1 input100  input-lg" placeholder="ESCOLARIDADE">

									</div>
								</div>
							</div>

							<div class="clearfix"></div>

							<div class="top5">
								<div class="col-xs-6">
									<div class="form-group input100 has-feedback">
										<input type="text" name="cargo" class="form-control fundo-form1 input100  input-lg" placeholder="CARGO" >

									</div>
								</div>

								<div class="col-xs-6">
									<div class="form-group input100 has-feedback ">
										<input type="text" name="area" class="form-control fundo-form1 input100  input-lg" placeholder="AREA">

									</div>
								</div>
							</div>

							<div class="clearfix"></div>

							<div class="top5">

								<div class="col-xs-6">
									<div class="form-group input100 has-feedback ">
										<input type="text" name="cidade" class="form-control fundo-form1 input100  input-lg" placeholder="CIDADE">

									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group input100 has-feedback ">
										<input type="text" name="estado" class="form-control fundo-form1 input100  input-lg" placeholder="ESTADO">

									</div>
								</div>

							</div>


							<div class="clearfix"></div>

							<div class="top5">
								<div class="col-xs-12">
									<div class="form-group input100 has-feedback ">
										<input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">

									</div>
								</div>
							</div>

							<div class="clearfix"></div>


							<div class="top5">
								<div class="col-xs-12">
									<div class="form-group input100 has-feedback">
										<textarea name="mensagem" cols="30" rows="4" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>

									</div>
								</div>
							</div>



							<div class="col-xs-12 text-right">
								<div class="top5 bottom5">
									<button type="submit" class="btn btn_formulario" name="btn_contato">
										ENVIAR
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<!--  ==============================================================  -->
				<!-- FORMULARIO-->
				<!--  ==============================================================  -->



			</div>
		</div>

		<div class="clearfix"></div>
		<div class="top50"></div>
	</div>
</div>

<!-- ======================================================================= -->
<!-- DESC EMPRESA  -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
<div class='container'>
	<div class="row mapa_trabalhe">
		<iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="508" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>
<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>



<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->




<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{

  if(!empty($_FILES[curriculo][name])):
    $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
    $texto = "Anexo: ";
    $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
    $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
  endif;

  $texto_mensagem = "
  Nome: ".$_POST[nome]." <br />
  Telefone: ".$_POST[telefone]." <br />
  Email: ".$_POST[email]." <br />
  Escolaridade: ".$_POST[escolaridade]." <br />
  Cargo: ".$_POST[cargo]." <br />
  Área: ".$_POST[area]." <br />
  Cidade: ".$_POST[cidade]." <br />
  Estado: ".$_POST[estado]." <br />
  Mensagem: <br />
  ".nl2br($_POST[mensagem])."

  <br><br>
  $texto
  ";

  Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), utf8_decode($_POST[email]));
  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), utf8_decode($_POST[email]));
  Util::alert_bootstrap("Obrigado por entrar em contato.");
  unset($_POST);
}

?>




<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Informe nome.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'insira email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu numero.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione sua Cidade.'
          }
        }
      },
      estado: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu Estado.'
          }
        }
      },
      escolaridade1: {
        validators: {
          notEmpty: {
            message: 'Sua Informação Acadêmica.'
          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
            maxSize: 5*1024*1024,   // 5 MB
            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
          }
        }
      },
      mensagem1: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
