
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 140px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- TITULO EMPRESA  -->
	<!-- ======================================================================= -->
	<div class="container">
		<div class="row top50 bottom40">
			<div class="col-xs-12 text-center titulo_fale">
				<img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_fale.png" alt="">
				<h4>TIRE SUAS DÚVIDAS, FALE CONOSCO</h4>
				<img class="top20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/arrow_donw_geral.png" alt="">
			</div>
		</div>

	</div>
</div>
<!-- ======================================================================= -->
<!-- TITULO EMPRESA  -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- DESC EMPRESA  -->
<!-- ======================================================================= -->
<div class="container">
	<div class="row dicas_geral_1">

		<div class="col-xs-12">
			<div class="dicas_geral pb20">
				<!--  ==============================================================  -->
				<!-- CONTATOS E ENDERENÇO-->
				<!--  ==============================================================  -->
				<div class="col-xs-12 top15">
					<div class="media-left media-middle ">
						<h2 class="media-heading">
							<span><?php Util::imprime($config[ddd1]) ?></span>
							<?php Util::imprime($config[telefone1]) ?>
						</h2>
					</div>
					<div class="media-body">
						<a href="tel:+55<?php Util::imprime($config[ddd1]) ?> <?php Util::imprime($config[telefone1]) ?>">
							<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn_chamar.jpg" alt="">
						</a>
					</div>
				</div>

				<div class="col-xs-2 top20">
					<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_mapa.png" alt="">
				</div>
				<div class="col-xs-10 top15">
					<div class="media-left media-middle ">
						<p class="media-heading">

							<?php Util::imprime($config[endereco]) ?>
						</p>
					</div>
					<div class="media-body">
						<a href="<?php Util::imprime($config[link_place]); ?>" class="" target="_blank">
							<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/abrir_mapa.jpg" alt="">
						</a>
					</div>
				</div>
				<!--  ==============================================================  -->
				<!-- CONTATOS E ENDERENÇO-->
				<!--  ==============================================================  -->
				<!--  ==============================================================  -->
				<!-- FORMULARIO-->
				<!--  ==============================================================  -->
				<div class="col-xs-12 padding0 top20">
					<h3 class="left15"><span>ENVIE  UM E-MAIL</span></h3>
					<form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
						<div class="fundo_formulario">
							<!-- formulario orcamento -->
							<div class="top10">
								<div class="col-xs-6">
									<div class="form-group input100 has-feedback">
										<input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">

									</div>
								</div>

								<div class="col-xs-6">
									<div class="form-group  input100 has-feedback">
										<input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">

									</div>
								</div>
							</div>

							<div class="clearfix"></div>


							<div class="top0">
								<div class="col-xs-6">
									<div class="form-group  input100 has-feedback">
										<input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">

									</div>
								</div>

								<div class="col-xs-6">
									<div class="form-group  input100 has-feedback">
										<input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">

									</div>
								</div>
							</div>

							<div class="clearfix"></div>

							<div class="">
								<div class="col-xs-12">
									<div class="form-group input100 has-feedback">
										<textarea name="mensagem" cols="25" rows="6" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>

									</div>
								</div>
							</div>

							<div class="col-xs-6 top30">

							</div>
							<div class="col-xs-6 text-right">
								<div class="top15 bottom10">
									<button type="submit" class="btn btn_formulario" name="btn_contato">
										ENVIAR
									</button>
								</div>
							</div>

						</div>
					</form>
				</div>
				<!--  ==============================================================  -->
				<!-- FORMULARIO-->
				<!--  ==============================================================  -->



			</div>
		</div>

		<div class="clearfix"></div>
		<div class="top50"></div>
	</div>
</div>

<!-- ======================================================================= -->
<!-- DESC EMPRESA  -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
<div class='container'>
	<div class="row mapa">
		<iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="508" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>
<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>



<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->




<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{
	$texto_mensagem = "
	Nome: ".($_POST[nome])." <br />
	Localidade: ".($_POST[localidade])." <br />
	Telefone: ".($_POST[telefone])." <br />
	Email: ".($_POST[email])." <br />
	Mensagem: <br />
	".(nl2br($_POST[mensagem]))."
	";


	Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), utf8_decode($_POST[email]));
  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), utf8_decode($_POST[email]));

	Util::alert_bootstrap("Obrigado por entrar em contato.");
	unset($_POST);
}


?>



<script>
$(document).ready(function() {
	$('.FormContatos').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'fa fa-check',
			invalid: 'fa fa-remove',
			validating: 'fa fa-refresh'
		},
		fields: {
			nome: {
				validators: {
					notEmpty: {
						message: 'Insira seu nome.'
					}
				}
			},
			email: {
				validators: {
					notEmpty: {
						message: 'Informe um email.'
					},
					emailAddress: {
						message: 'Esse endereço de email não é válido'
					}
				}
			},
			telefone: {
				validators: {
					notEmpty: {
						message: 'Por favor informe seu numero!.'
					},
					phone: {
						country: 'BR',
						message: 'Informe um telefone válido.'
					}
				},
			},
			assunto1: {
				validators: {
					notEmpty: {

					}
				}
			}
		}
	});
});
</script>
