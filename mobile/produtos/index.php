
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 140px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	  <!-- ======================================================================= -->
	  <!-- TITULO PRODUTOS  -->
	  <!-- ======================================================================= -->
	  <div class="container">
	    <div class="row top50 bottom40">
	      <div class="col-xs-12 text-center">
	        <img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_produtos.png" alt="">
	        <h4>CONHECA NOSSOS PRODUTOS</h4>
	        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/arrow_donw_geral.png" alt="">
	      </div>
	    </div>

	  </div>
	</div>
	<!-- ======================================================================= -->
	<!-- TITULO PRODUTOS  -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- DESC PRODUTOS  -->
	<!-- ======================================================================= -->
	<div class="container">
	  <div class="row dicas_geral_1">

	    <div class="col-xs-12">
				<div class="dicas_geral pb20">
					<div class="sombra_dicas_geral_dicas"></div>

					<?php
		      $ids = explode("/", $_GET[get1]);


			      //  FILTRA AS CATEGORIAS
			       if (!empty( $ids[0] )) {
			          $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $ids[0]);
			          $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
			       }


			       //  FILTRA AS CATEGORIAS
			       if (isset( $ids[1] )) {
			          $id_subcategoria = $obj_site->get_id_url_amigavel("tb_subcategorias_produtos", "idsubcategoriaproduto", $ids[1]);
			          $complemento .= "AND id_subcategoriaproduto = '$id_subcategoria' ";
			       }





			     //  FILTRA PELO TITULO
			       if(isset($_POST[busca_topo])):
			         $complemento .= "AND titulo LIKE '%$_POST[busca_topo]%'";
			       endif;



		      $result = $obj_site->select("tb_produtos", $complemento);
		      if(mysql_num_rows($result) == 0){
		         echo "<h2 class='bg-info' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
		      }else{
		        $i = 0;
		        while($row = mysql_fetch_array($result))
		        {
		          ?>
							<div class="col-xs-12 top15 text-center">
								<a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
									<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 450,251, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>

								</a>
							</div>

							<div class="col-xs-12 dicas_descricao_home">
								<div class="top15">
									<h1><?php Util::imprime($row[titulo]) ?></h1>
								</div>
								<div class="top10">
									<p><?php Util::imprime($row[descricao], 300) ?></p>
								</div>
							</div>

							<div class="produto-hover text-center">

								<div class="col-xs-4 top15">
									<a class="btn btn_saiba_mais_produtos" href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
										SAIBA MAIS
									</a>
								</div>
								<div class="col-xs-8 top15">
									<a class="btn btn_saiba_mais_produtos" href="javascript:void(0);" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
										ADICIONAR AO ORÇAMENTO
									</a>
								</div>
							</div>

							<?php
						}
					}
					?>

	    </div>
			</div>

	    <div class="clearfix"></div>
	    <div class="top50"></div>
	  </div>
	</div>

	<!-- ======================================================================= -->
	<!-- DESC PRODUTOS  -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>




<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
