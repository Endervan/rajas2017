
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 140px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	  <!-- ======================================================================= -->
	  <!-- TITULO EMPRESA  -->
	  <!-- ======================================================================= -->
	  <div class="container">
	    <div class="row top50 bottom40">
	      <div class="col-xs-12 text-center">
	        <img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_empresa.png" alt="">
	        <h4>CONHEÇA MAIS NOSSA EMPRESA</h4>
	        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/arrow_donw_geral.png" alt="">
	      </div>
	    </div>

	  </div>
	</div>
	<!-- ======================================================================= -->
	<!-- TITULO EMPRESA  -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- DESC EMPRESA  -->
	<!-- ======================================================================= -->
	<div class="container">
	  <div class="row ">

	    <div class="col-xs-12">
				<div class="dicas_geral pg10">
					<div class="sombra_dicas_geral"></div>
	      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>

	      <div class="top15">
	        <p><?php Util::imprime($row[descricao]); ?></p>
	      </div>
	    </div>
			</div>

	    <div class="clearfix"></div>
	    <div class="top50"></div>
	  </div>
	</div>

	<!-- ======================================================================= -->
	<!-- DESC EMPRESA  -->
	<!-- ======================================================================= -->


	<div class="container">
	  <div class="row ">

	    <!-- ======================================================================= -->
	    <!-- MISSÃO  -->
	    <!-- ======================================================================= -->
	    <div class="col-xs-12">
	      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
	      <div class="top15">
	        <h3><?php Util::imprime($row[titulo]); ?></h3>
	      </div>
	      <div class="top15">
	        <p><?php Util::imprime($row[descricao]); ?></p>
	      </div>

	    <!-- ======================================================================= -->
	    <!-- MISSÃO  -->
	    <!-- ======================================================================= -->

	    <!-- ======================================================================= -->
	    <!-- visão  -->
	    <!-- ======================================================================= -->

	      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
	      <div class="top15">
	        <h3><?php Util::imprime($row[titulo]); ?></h3>
	      </div>
	      <div class="top15">
	        <p><?php Util::imprime($row[descricao]); ?></p>
	      </div>

	    <!-- ======================================================================= -->
	    <!-- visão  -->
	    <!-- ======================================================================= -->

	    <!-- ======================================================================= -->
	    <!-- valores  -->
	    <!-- ======================================================================= -->

	      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
	      <div class="top15">
	        <h3><?php Util::imprime($row[titulo]); ?></h3>
	      </div>
	      <div class="top15">
	        <p><?php Util::imprime($row[descricao]); ?></p>
	      </div>

	      <?php
	      $result = $obj_site->select("tb_atuacoes","LIMIT 5");
	      if(mysql_num_rows($result) > 0){
	        while($row = mysql_fetch_array($result)){
	          ?>
	          <div class="col-xs-12 padding0  top20 lato_black">
	            <p><i class="fa fa-check-square-o right10" aria-hidden="true"></i><?php Util::imprime($row[titulo]); ?></p>
	          </div>
	          <?php
	        }
	      }
	      ?>

	  </div>
	    <!-- ======================================================================= -->
	    <!-- valores  -->
	    <!-- ======================================================================= -->


	    <div class="clearfix"></div>
	    <div class="top20"></div>
	  </div>
	</div>



	<!-- ======================================================================= -->
    <!--NOSSOS FORNECEDORES -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row">
        <div class="col-xs-12 top20 bottom15">
          <h3 class="left15">NOSSOS FORNECEDORES</h3>
          <!-- ======================================================================= -->
          <!--SLIDER FORNECEDORES -->
          <!-- ======================================================================= -->
          <?php require_once('../includes/slider_fornecedores.php'); ?>
          <!-- ======================================================================= -->
          <!--SLIDER FORNECEDORES -->
          <!-- ======================================================================= -->
        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- NOSSOS FORNECEDORES -->
    <!-- ======================================================================= -->



	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>




<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
