
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 10); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 140px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- TITULO EMPRESA  -->
	<!-- ======================================================================= -->
	<div class="container">
		<div class="row top50 bottom40">
			<div class="col-xs-12 text-center">
				<img class="bottom20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_dicas.png" alt="">
				<h4>CONHECA NOSSAS DICAS</h4>
				<img class="top20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/arrow_donw_geral.png" alt="">
			</div>
		</div>

	</div>
</div>
<!-- ======================================================================= -->
<!-- TITULO EMPRESA  -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- DESC EMPRESA  -->
<!-- ======================================================================= -->
<div class="container">
	<div class="row dicas_geral_1">

		<div class="col-xs-12">
			<div class="dicas_geral pb20">
				<div class="sombra_dicas_geral_dicas"></div>

				<?php
				$result = $obj_site->select("tb_dicas","ORDER BY RAND()");
				if(mysql_num_rows($result) > 0){
					while($row = mysql_fetch_array($result)){
						?>
						<div class="col-xs-6 top15 dicas_home bottom20">
							<a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
								<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 201, 94, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
								<div class="lista-dica-faixa"></div>
							</a>
							<div class="lista_dica_desc top15">
								<h1><?php Util::imprime($row[titulo]); ?></h1>
							</div>
						</div>
						<?php
					}
				}
				?>

			</div>
		</div>

		<div class="clearfix"></div>
		<div class="top50"></div>
	</div>
</div>

<!-- ======================================================================= -->
<!-- DESC EMPRESA  -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>



<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
