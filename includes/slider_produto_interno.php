<!-- ======================================================================= -->
<!-- SLIDER produtos internos -->
<!-- ======================================================================= -->
<div class="col-xs-12 slider_prod_geral">
  <div class="top20">
    <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
  </div>
  <!-- Place somewhere in the <body> of your page -->
  <div id="slider" class="flexslider">
    <ul class="slides slider-prod">
      <?php
      $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
      if(mysql_num_rows($result) == 0){
        echo "<h2 class='bg-info top25 ' style='padding: 20px; color:#000;'>Nenhum produto(s) encontrado.</h2>";

      }else{
          while($row = mysql_fetch_array($result)){
          ?>
          <li class="zoom">
            <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $row[imagem]; ?>" class="group3">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 400, 350, array("class"=>"input100", "alt"=>"")) ?>
            </a>
          </li>
          <?php
        }
      }
      ?>
      <!-- items mirrored twice, total of 12 -->
    </ul>
  </div>
  <div id="carousel" class="flexslider">
    <ul class="slides slider-prod-tumb">
      <?php
      $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
      if(mysql_num_rows($result) > 0)
      {
        while($row = mysql_fetch_array($result)){
          ?>
          <li>
            <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" />

          </li>
          <?php
        }
      }
      ?>
      <!-- items mirrored twice, total of 12 -->
    </ul>
  </div>


  
</div>
<!-- ======================================================================= -->
<!-- SLIDER produtos internos -->
<!-- ======================================================================= -->
