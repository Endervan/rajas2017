<?php
$qtd_itens_linha = 1; // quantidade de itens por linha
$id_slider = "carousel-example-generic-servicos";
unset($linha);
?>


<div id="<?php echo $id_slider ?>" class="carousel slide top20 slider-parceiros-servicos" data-ride="carousel" interval="555" pause="hover">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
    $result = $obj_site->select("tb_galerias_produtos","and id_produto = $dados_dentro[idproduto] order by rand()");
    $linhas = mysql_num_rows($result) / $qtd_itens_linha;
    $b_temp = 0;

    for($i_temp = 0; $i_temp < $linhas; $i_temp++){
      ?>
      <li data-target="#<?php echo $id_slider ?>" data-slide-to="<?php echo $i_temp; ?>" class="<?php if($i_temp == 0){ echo "active"; } ?>"></li>
      <?php


      // guarda a qtd de linhas
      $result1 = $obj_site->select("tb_galerias_produtos", "and id_produto = $dados_dentro[idproduto] limit $b_temp, $qtd_itens_linha");

      if(mysql_num_rows($result1) > 0){
        while($row1 = mysql_fetch_array($result1)){
          $linha[] = $row1;
        }
      }

      //  aumenta a qtd de itens a busca na consulta
      $b_temp = $b_temp + $qtd_itens_linha;
    }

    ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">

    <?php
    if (count($linhas) > 0) {
      $i_temp = 0;
      $b_temp = $qtd_itens_linha;
      $c_temp = 0;
      for($i_temp = 0; $i_temp < $linhas; $i_temp++){
        ?>
        <div class="item <?php if($i_temp == 0){ echo "active"; } ?>">

          <?php

          //  lista os itens da linha
          for($c_temp; $c_temp <$b_temp; $c_temp++){
            $imagem = $linha[$c_temp][imagem];

            if(!empty($imagem)):
              ?>

              <div class="lista_parceiros_clientes">
                <div class="col-xs-2 div_personalizada text-center">
                  <!-- <img src="<?php //echo Util::caminho_projeto() ?>/uploads/<?php //Util::imprime($imagem); ?>" alt="" /> -->
                  <div class="bg_imagem">
                    <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $imagem; ?>" class="group3">
                      <?php $obj_site->redimensiona_imagem("../uploads/$imagem", 960,394, array("class"=>"", "alt"=>"$linha[$c_temp][titulo]") ) ?>
                    </a>
                  </div>
                </div>
                <div class="col-xs-12 lato_black">
                  <h1><?php Util::imprime($linha[$c_temp][titulo]) ?></h1>
                </div>
                <div class="col-xs-12 dicas_descricao_home">
                  <p><?php Util::imprime($linha[$c_temp][descricao], 500) ?></p>
                </div>

              </div>

              <?php
            endif;
          }
          ?>

        </div>
        <?php
        $b_temp = $b_temp + $qtd_itens_linha;
      }
    }
    ?>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#<?php echo $id_slider ?>" role="button" data-slide="prev">
    <img src="<?php echo Util::caminho_projeto() ?>/imgs/arrow_right_grande.png" alt="">
  </a>
  <a class="right carousel-control" href="#<?php echo $id_slider ?>" role="button" data-slide="next">
    <img src="<?php echo Util::caminho_projeto() ?>/imgs/arrow_left_grande.png" alt="">
  </a>
</div>
