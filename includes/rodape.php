<div class="clearfix"></div>
<div class="container-fluid rodape">
	<div class="row">

<a href="#" class="scrollup">scrollup</a>
		<div class="container top40">
			<div class="row">

				<!-- ======================================================================= -->
				<!-- MENU    -->
				<!-- ======================================================================= -->
				<div class="col-xs-12">
					<div class="barra_branca">
					<ul class="menu-rodape">
						<li><a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dicas"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a></li>
					</ul>
					<a href="http://masmidia.com.br/clientes/rajas2017/admin/"  target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/portal_cliente_rodape.png" alt="">
					</a>
					</div>
				</div>
				<!-- ======================================================================= -->
				<!-- MENU    -->
				<!-- ======================================================================= -->


				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-xs-2 top10 bottom15">
					<a href="#">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="" />
					</a>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->



				<!-- ======================================================================= -->
				<!-- ENDERECO E TELEFONES    -->
				<!-- ======================================================================= -->
				<div class="col-xs-8 top35 telefone_rodape">
					<p class="bottom15"><i class="fa fa-home right15"></i><?php Util::imprime($config[endereco]); ?></p>
					<p class="bottom15">
						<i class="fa fa-phone right15"></i>
						<?php Util::imprime($config[ddd1]); ?>
						<?php Util::imprime($config[telefone1]); ?>
                        <span class="left15"></span>
						<?php if (!empty($config[telefone2])) { ?>
							 <?php Util::imprime($config[ddd2]); ?>
							 <?php Util::imprime($config[telefone2]); ?>
						<?php } ?>

						<span class="left15"></span>
						<?php if (!empty($config[telefone3])) { ?>
							 <?php Util::imprime($config[ddd3]); ?>
							 <?php Util::imprime($config[telefone3]); ?>
						<?php } ?>

						<span class="left15"></span>
						<?php if (!empty($config[telefone4])) { ?>
							 <?php Util::imprime($config[ddd4]); ?>
							 <?php Util::imprime($config[telefone4]); ?>
						<?php } ?>

					</p>
					<p><i class="fa fa-envelope right15"></i>rajas@rajasesquadrias.com.br</p>
				</div>
				<!-- ======================================================================= -->
				<!-- ENDERECO E TELEFONES    -->
				<!-- ======================================================================= -->


				<div class="col-xs-2 text-right top25">

					<?php if ($config[facebook] != "") { ?>
					<a href="<?php Util::imprime($config[facebook]); ?>" title="Google Plus" target="_blank" >
						<i class="fa fa-facebook right15 icon-rodape"></i>
					</a>
					<?php } ?>

					<?php if ($config[twitter] != "") { ?>
					<a href="<?php Util::imprime($config[twitter]); ?>" title="Google Plus" target="_blank" >
						<i class="fa fa-twitter right15 icon-rodape"></i>
					</a>
					<?php } ?>

					<?php if ($config[instagram] != "") { ?>
					<a href="<?php Util::imprime($config[instagram]); ?>" title="Google Plus" target="_blank" >
						<i class="fa fa-instagram right15 icon-rodape"></i>
					</a>
					<?php } ?>

					<?php if ($config[google_plus] != "") { ?>
					<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
						<i class="fa fa-google-plus right15 icon-rodape"></i>
					</a>
					<?php } ?>

					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
					</a>
				</div>

			</div>
		</div>
	</div>
</div>






<div class="container-fluid">
	<div class="row rodape-preto">
		<div class="col-xs-12 text-center top15 bottom15">
			<h5>© Copyright RAJAS ESQUADRIAS EM ALUMINÍO </h5>
		</div>
 </div>
</div>
