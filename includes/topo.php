
<div class="container">
  <div class="row icon_telefone top8">



    <div class="col-xs-9">
      <div class="pull-right">
        <div class="media-left media-middle ">
          <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_topo_telefones.png" alt="">
        </div>
        <div class="media-body">
          <h2 class="media-heading">
            <span><?php Util::imprime($config[ddd1]) ?></span>
            <?php Util::imprime($config[telefone1]) ?>
          </h2>

        </div>
        <?php if (!empty($config[telefone2])): ?>
          <div class="media-body ">

            <h2 class="media-heading left10">
              <span><?php Util::imprime($config[ddd2]) ?></span>
              <?php Util::imprime($config[telefone2]) ?>
            </h2>
          <?php endif; ?>

        </div>



      </div>
      <div class="col-xs-3 icon_telefone padding0">
        <a href="<?php echo Util::caminho_projeto() ?>/obras"  title="portal do cliente">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/portal_cliente.jpg" alt="portal do cliente" class="input100">
        </a>
      </div>
    </div>



</div>
</div>

<div class="container-fluid top8">
  <div class="row topo">

    <div class="container">
      <div class="row">

        <div class="col-xs-2 logo">
          <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo2.png" alt="início" class="">
          </a>
        </div>


        <!--  ==============================================================  -->
        <!-- MENU-->
        <!--  ==============================================================  -->
        <div class="col-xs-8 menu">
          <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
          <div id="navbar">
            <ul class="nav navbar-nav navbar-right lato_black">
              <li >
                <a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
                  href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
                </li>

                <li >
                  <a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA</a>
                  </li>

                  <li>
                    <a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "produtos-categoria" or Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a>
                    </li>


                    <li >
                      <a class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>"
                        href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a>
                      </li>

                      <li >
                        <a class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>"
                          href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a>
                        </li>

                        <li >
                          <a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>"
                            href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a>
                          </li>

                        </ul>

                      </div><!--/.nav-collapse -->
                    </div>
                    <!--  ==============================================================  -->
                    <!-- MENU-->
                    <!--  ==============================================================  -->



                    <!--  ==============================================================  -->
                    <!--CARRINHO-->
                    <!--  ==============================================================  -->
                    <div class="col-xs-2 padding0 top25">
                      <div class="pull-right dropdown">
                        <a class="btn_topo" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <img src="<?php echo Util::caminho_projeto(); ?>/imgs/btn_ocamento.png" alt="" />
                          <span class="badge"><?php echo count($_SESSION[solicitacoes_produtos])+count($_SESSION[solicitacoes_servicos]); ?></span>

                        </a>


                        <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dropdownMenu1">

                          <!--  ==============================================================  -->
                          <!--sessao adicionar produtos-->
                          <!--  ==============================================================  -->
                          <?php
                          if(count($_SESSION[solicitacoes_produtos]) > 0)
                          {
                            for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                            {
                              $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                              ?>
                              <div class="lista-itens-carrinho">
                                <div class="col-xs-2">
                                  <?php if(!empty($row[imagem])): ?>
                                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                  <?php endif; ?>
                                </div>
                                <div class="col-xs-8">
                                  <h1><?php Util::imprime($row[titulo]) ?></h1>
                                </div>
                                <div class="col-xs-1">
                                  <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                                </div>
                                <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>
                              </div>
                              <?php
                            }
                          }
                          ?>


                          <!--  ==============================================================  -->
                          <!--sessao adicionar servicos-->
                          <!--  ==============================================================  -->
                          <?php
                          if(count($_SESSION[solicitacoes_servicos]) > 0)
                          {
                            for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                            {
                              $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                              ?>
                              <div class="lista-itens-carrinho">
                                <div class="col-xs-2">
                                  <?php if(!empty($row[imagem])): ?>
                                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                  <?php endif; ?>
                                </div>
                                <div class="col-xs-8">
                                  <h1><?php Util::imprime($row[titulo]) ?></h1>
                                </div>
                                <div class="col-xs-1">
                                  <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                                </div>
                                <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>

                              </div>
                              <?php
                            }
                          }
                          ?>




                          <div class="col-xs-12 text-right top10 bottom20">
                            <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn-danger" >
                              ENVIAR ORÇAMENTO
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--  ==============================================================  -->
                    <!--CARRINHO-->
                    <!--  ==============================================================  -->






                  </div>
                </div>

              </div>
            </div>
